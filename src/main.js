import { createApp } from 'vue'
import App from './App.vue'
import activeelement from './components/ActiveElement.vue'
import knowledgebase from './components/KnowledgeBase.vue'
import knowledgeelement from './components/knowledgeElement.vue'
import knowledgegrid from './components/knowledgeGrid.vue'
const app = createApp(App);
app.component('active-element',activeelement);
app.component('knowledge-base',knowledgebase);
app.component('knowledge-element',knowledgeelement);
app.component('knowledge-grid',knowledgegrid);

app.mount('#app');
